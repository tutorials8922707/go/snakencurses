package main

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rthornton128/goncurses"

	"gitlab.com/tutorials8922707/go/snakencurses/board"
	"gitlab.com/tutorials8922707/go/snakencurses/game"
)

func init_ncurses() {
	_, err := goncurses.Init()
	if err != nil {
		panic(err)
	}
	goncurses.CBreak(true)
	goncurses.Echo(false)
	err = goncurses.Cursor(0)
	if err != nil {
		panic(err)
	}
}

func init_logging() *os.File {
	// UNIX Time is faster and smaller than most timestamps
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
  f, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
  if err != nil {
    panic(err)
  }
  log.Logger = log.Output(f)
  return f
}

func main() {
	init_ncurses()

	snakeBoard := board.Board{}
	snakeBoard.Initialize()

  logFile := init_logging()
  defer logFile.Close()

	snakeGame := game.NewGame(&snakeBoard)
  snakeGame.StartGame()

	for {
		key := snakeBoard.Win.GetChar()
		if key == goncurses.KEY_RIGHT {
			snakeGame.CurrentDirection = game.RIGHT
		} else if key == goncurses.KEY_LEFT {
			snakeGame.CurrentDirection = game.LEFT
		} else if key == goncurses.KEY_UP {
			snakeGame.CurrentDirection = game.UP
		} else if key == goncurses.KEY_DOWN {
			snakeGame.CurrentDirection = game.DOWN
		} else if key == 'q' {
       break
    }
		snakeGame.MakeStep()

		snakeBoard.Win.Refresh()
    if snakeGame.CheckGameOver() {
      break
    }
	}

	goncurses.End()

  fmt.Println("Game Over")
}
