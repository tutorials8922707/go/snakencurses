package board

import (
	"github.com/rthornton128/goncurses"
)

type Board struct {
  FieldHeight int
  FieldWidth int
	Win  *goncurses.Window
}

func (b *Board) Initialize() {
	// goncurses.StdScr().MaxYX()
	var err error
  b.FieldHeight = 30
  b.FieldWidth = 50
	b.Win, err = goncurses.NewWindow(b.FieldHeight, b.FieldWidth, 3, 3)
	if err != nil {
		panic(err)
	}
	err = b.Win.Box('|', '-')
	if err != nil {
		panic(err)
	}
	goncurses.StdScr().Refresh()
  err = b.Win.Keypad(true)
  b.Win.Timeout(1000)
  if err != nil {
    panic(err)
  }
	b.Win.Refresh()
}
