module gitlab.com/tutorials8922707/go/snakencurses

go 1.22.3

require (
	github.com/rs/zerolog v1.32.0
	github.com/rthornton128/goncurses v0.0.0-20231014161942-82671379df88
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
