package game

import (
	"math/rand"

	"github.com/rs/zerolog/log"

	"gitlab.com/tutorials8922707/go/snakencurses/board"
)

type Direction = int8

const (
	RIGHT Direction = 0
	LEFT  Direction = 1
	UP    Direction = 2
	DOWN  Direction = 3
)

type Cord struct {
	y  int
	x  int
	ch string
}

type Snake struct {
	Cords []Cord
}

type Game struct {
	board            *board.Board
	Snake            Snake
	Fruit            Cord
	CurrentDirection Direction
}

func NewGame(board *board.Board) *Game {
	g := &Game{}
	g.board = board
	g.Snake.Cords = []Cord{{y: 5 - 1, x: 1, ch: "s"}, {y: 5 - 1, x: 2, ch: "s"}, {y: 5 - 1, x: 3, ch: "s"}}
	g.CurrentDirection = RIGHT
	return g
}

func (g *Game) StartGame() {
	g.DrawFruit()
	g.DrawSnake()
}

func (g *Game) createFruit() {
	y := rand.Int() % g.board.FieldHeight
	x := rand.Int() % g.board.FieldWidth
	g.Fruit = Cord{y: y, x: x, ch: "O"}
	for i := 0; i < len(g.Snake.Cords); i++ {
		if g.Fruit.y == g.Snake.Cords[i].y &&
			g.Fruit.x == g.Snake.Cords[i].x &&
			g.Fruit.y == 0 && g.Fruit.x == 0 &&
			g.Fruit.x == g.board.FieldWidth &&
			g.Fruit.y == g.board.FieldHeight {
			g.createFruit()
		}
	}
}

func (g *Game) DrawFruit() {
	g.createFruit()
	g.board.Win.MovePrint(g.Fruit.y, g.Fruit.x, g.Fruit.ch)
}

func (g *Game) DrawSnake() {
	for i := 0; i < len(g.Snake.Cords); i++ {
		log.Info().Msgf("drawwing: %v", g.Snake.Cords[i])
		char := g.Snake.Cords[i].ch
		if i == len(g.Snake.Cords)-1 {
			char = "S"
		}
		g.board.Win.MovePrint(g.Snake.Cords[i].y, g.Snake.Cords[i].x, char)
	}
	log.Info().Msg("END drawwing")
}

func (g *Game) eatFruit() {
	if g.Fruit.y == g.Snake.Cords[len(g.Snake.Cords)-1].y && g.Fruit.x == g.Snake.Cords[len(g.Snake.Cords)-1].x {
		lastCort := g.Snake.Cords[len(g.Snake.Cords)-1]
		lastCort.ch = "o"
		g.Snake.Cords = append(g.Snake.Cords, lastCort)
		g.DrawFruit()
	}
}

func (g *Game) moveSnake() {
	g.board.Win.MovePrint(g.Snake.Cords[0].y, g.Snake.Cords[0].x, " ")
	g.Snake.Cords = g.Snake.Cords[1:]
	if g.CurrentDirection == RIGHT {
		g.Snake.Cords = append(g.Snake.Cords, Cord{y: g.Snake.Cords[len(g.Snake.Cords)-1].y, x: g.Snake.Cords[len(g.Snake.Cords)-1].x + 1, ch: "s"})
	} else if g.CurrentDirection == LEFT {
		g.Snake.Cords = append(g.Snake.Cords, Cord{y: g.Snake.Cords[len(g.Snake.Cords)-1].y, x: g.Snake.Cords[len(g.Snake.Cords)-1].x - 1, ch: "s"})
	} else if g.CurrentDirection == UP {
		g.Snake.Cords = append(g.Snake.Cords, Cord{y: g.Snake.Cords[len(g.Snake.Cords)-1].y - 1, x: g.Snake.Cords[len(g.Snake.Cords)-1].x, ch: "s"})
	} else if g.CurrentDirection == DOWN {
		g.Snake.Cords = append(g.Snake.Cords, Cord{y: g.Snake.Cords[len(g.Snake.Cords)-1].y + 1, x: g.Snake.Cords[len(g.Snake.Cords)-1].x, ch: "s"})
	}
}

func (g *Game) MakeStep() {
	g.moveSnake()
	g.eatFruit()
	g.DrawSnake()
}

func (g *Game) CheckGameOver() bool {
	if g.Snake.Cords[0].x == 0 || g.Snake.Cords[0].x == g.board.FieldWidth || g.Snake.Cords[0].y == 0 || g.Snake.Cords[0].y == g.board.FieldHeight {
		return true
	}
	for i := 0; i < len(g.Snake.Cords)-2; i++ {
		if g.Snake.Cords[len(g.Snake.Cords)-1].x == g.Snake.Cords[i].x && g.Snake.Cords[len(g.Snake.Cords)-1].y == g.Snake.Cords[i].y {
			return true
		}
	}
	return false
}
